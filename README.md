# fh/git-hooks

## Project Summary

This project is intended to be used in conjunction with the [fh/git-deployment](https://bitbucket.org/fhcode/git-deployment) package for situations where a fork of fh/git-deployment is your best option rather than a contribution. (See the fh/git-deployment documentation for more guidance on how to make that choice.)

This package provides a simple framework for making changes to your git-deployment.phar file, testing them quickly, and re-deploying those chagnes to a new version of your git-deployment.phar file which should be installed globally on your development server.

It also provides a sample post-receive hook that you can quickly configure and copy into the hooks directory of any git repository.

## Pre-requisites

The Makefile assumes that you have composer installed globally because you will need it to install packages that git-deployment depends on. It also uses the location of your globally installed composer file to determine where git-deployment should be installed. If you do not have composer installed, the make process will fail.

## Instructions

When working with this project, set up a development gitolite server locally and log-in as the git user to clone this repository and begin making changes. Give the git user sudo access so you can copy files to the /usr/local/bin directory, or wherever you have composer installed.

#### The post-receive file

This file provides a starting point with all environment variables set up as examples for a standard beta auto-deployment. If you are logged in as the git user on your server, then you should be able to copy this file directly into any project's hooks directory. These instructions presume a standard installation of gitolite, and that you are logged in as the gitolite user (git in most cases).

1. Modify your post-receive file for the project you want to auto-deploy whenever you push from your working clone to this repository.
2. Copy the post-receive file directly to ~/repositories/my-project.git/hooks/. as the git user (where my-project.git is the name of your repository):

```sh
$ cp post-receive ~/repositories/my-project.git/hooks/.
```

#### The Makefile

The Makefile provides all the make targets you need to test and deploy your modified version of git-deployment.

1. If you have made changes and created new unit tests, use:

```sh
$ make test
```

This will install all dev dependencies, including phpunit using composer and run your unit tests. If you would like to run a single test, simply change directories into the git-deployment submodule itself and run phpunit directly there with any command line arguments you wish.

2. If you would like to build and install your newly modified git-deployment phar file, use:

```sh
$ make all
```

This will destroy the vendor directory (with any dev dependencies) and re-install using composer's --no-dev option to only install the packages it needs to create the phar file. This significantly reduces both time to build and resulting phar file size. Then it will build your phar file and provide a sudo command that you can cut / paste to your command line to install it in the correct location.

#### Cloning and making changes to git-deployment from this project

The fh/git-deployment package is installed as a git submodule in this project. So all you have to do to pull this project down from the cloud is:

```sh
$ git submodule init
$ git submodule update
```

This will clone fh/git-deployment into a directory called git-deployment inside this project. Then you can make any changes you want to the fh/git-deployment package.

#### Testing your changes to git-deployment

If you create a new strategy or make changes to the deployment controller process inside Deploy.php, make sure you fix existing and write new unit tests for your updated deployment process so you know you are seeing the commands executed in the order you expect *before* you install your modified git-deployment.phar file. Extensive example unit tests have been provided in the *tests/* directory.

#### Environment variables

Since the production deployment process will depend on envinroment variables when the process is actually in use, phpunit also sets environment variables for testing purposes. See the phpunit.xml file for a list of environment variables used.

It is not recommended that you change the environment variables in the phpunit.xml file for testing new scenarios. Instead, simply change the configuration values in the $deploy->config array in your test case. Since the config array is a public property, you can set any of them as you like in the setUp() method of your phpunit test class or at the top of any given test method. See the existing unit tests for examples.

## Contributing

This project is provided as an example project as a starting point to facilitate your development process. If you make customizations to the process that you believe other people would benefit from, feel free to submit a pull request to the master branch. However keep in mind that whatever you submit should be helpful for example purposes and not unique to your process.