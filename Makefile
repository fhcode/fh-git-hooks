composerpath := $(shell which composer)
installpath := $(shell dirname $(composerpath))

all: submodule clean phar install

submodule:
	git submodule init; \
	git submodule update;

clean:
	if [ -d ./build ] ; then rm -rf ./build; fi;  \
	mkdir build; \
	if [ -d ./git-deployment/vendor ] ; then rm -rf git-deployment/vendor; fi;

phar:
	cd git-deployment; \
	composer install --no-dev; \
	cd ../; \
	phar-composer build git-deployment build/git-deployment.phar; \
	mv build/git-deployment.phar build/git-deployment; \
	chmod 777 build/git-deployment;

install:
	echo; \
	echo; \
	echo "To install, paste the following command into your terminal:"; \
	echo; \
	echo sudo cp build/git-deployment $(installpath)/git-deployment; \
	echo; \
	echo;

test:
	cd git-deployment; \
	composer install; \
	./vendor/bin/phpunit;

